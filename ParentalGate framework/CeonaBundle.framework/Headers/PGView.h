//
//  PGView.h
//  ParentalGate
//
//  Created by Rahul Singh on 03/09/13.
//  Copyright (c) 2013 Rahul Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

typedef enum {
    ParentalGateTypeNone = 0,
    ParentalGateTypeEnterNumber,
    ParentalGateTypeDivisibleBy3,
    ParentalGateType3FingerTap
}ParentalGateType;

@protocol ParentalLockSuccessDelegate;
@protocol ParentalLockShowDelegate;

@interface PGView : UIView{
    
}

@property (nonatomic , assign) id <ParentalLockSuccessDelegate>delegate;
@property (nonatomic , assign) id <ParentalLockShowDelegate>showDelegate;
@property(nonatomic , assign) ParentalGateType lockType;
@property (nonatomic , readonly) BOOL isShown;

+ (BOOL)isIPadDevice;
- (void)hide;
- (void)show;
- (id)initWithSize:(CGSize)size andParentalGate:(ParentalGateType)gateType;
@end


@protocol ParentalLockSuccessDelegate <NSObject>

- (void) ParentalLockSucceeded: (PGView *)sender;
- (void) ParentalLockCancelled: (PGView *)sender;

@end

@protocol ParentalLockShowDelegate <NSObject>

- (void) ParentalLockWillShow: (PGView *)sender;
- (void) ParentalLockDidShow: (PGView *)sender;
- (void) ParentalLockWillHide: (PGView *)sender;
- (void) ParentalLockDidHide: (PGView *)sender;

@end
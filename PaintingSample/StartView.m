//
//  StartView.m
//  PaintingSample
//
//  Created by prem on 10/5/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "StartView.h"
#import "PaintingSampleViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "EnchantedHelperClass.h"



@implementation StartView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    if ([EnchantedHelperClass isiPadDevice])
    {
        startButton.frame = CGRectMake(73*2, 72*2, 120, 120);
        smallLetterButton.frame = CGRectMake(181*2, 72*2, 120, 120);
    }
    [startButton.layer setBorderWidth:1.0];
    [startButton.layer setBorderColor:[[UIColor blackColor] CGColor]];
    [startButton.layer setShadowOffset:CGSizeMake(5, 5)];
    [startButton.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [startButton.layer setShadowOpacity:0.5];
    
    [smallLetterButton.layer setBorderWidth:1.0];
    [smallLetterButton.layer setBorderColor:[[UIColor blackColor] CGColor]];
    [smallLetterButton.layer setShadowOffset:CGSizeMake(5, 5)];
    [smallLetterButton.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [smallLetterButton.layer setShadowOpacity:0.5];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


-(IBAction)openGameController:(id)sender

{
    
    [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"chooseLetters"];
    [self presentPaintingView];
       

}

-(IBAction)smallLetterPressed:(id)sender
{
    
    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"chooseLetters"];
    [self presentPaintingView];
    

       
    
}

-(void)presentPaintingView
{
    viewController =[[PaintingSampleViewController alloc]initWithNibName:@"PaintingSampleViewController" bundle:Nil];
    //[self presentModalViewController:viewController animated:YES];
    [self.navigationController pushViewController:viewController animated:YES];
    [viewController release];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
   // return (interfaceOrientation == UIInterfaceOrientationPortrait);
     return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}




@end

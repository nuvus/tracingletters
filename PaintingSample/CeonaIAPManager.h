//
//  CeonaIAPManager.h
//  CustomIcons
//
//  Created by Rahul Singh on 26/09/13.
//
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@interface CeonaIAPManager : NSObject <SKProductsRequestDelegate , SKPaymentTransactionObserver>
{
    SEL IAPStartSelector;
    SEL IAPFinishSelector;
    BOOL failedTransactionPopuped;
}

@property (nonatomic , assign) id delegate;

@property (nonatomic, retain) NSArray *validProductsArray;

@property (nonatomic, retain) SKPaymentTransaction *paymentTransactionDetails;

@property (nonatomic, retain) SKProductsRequest *productsRequest;



//+ (instancetype)sharedInstance;//Implemented though not needed

+ (BOOL)canMakePurchases;

- (void)initiateInAppPurchaseWithIdentifierSet:(NSSet *)_productIdsSet withDelegate:(id)_delegate andStartSelector:(SEL)startSelector andFinishSelector:(SEL)finishSelector;

- (void)restoreInAppPurchasesDelegate:(id)_delegate andStartSelector:(SEL)startSelector andFinishSelector:(SEL)finishSelector;

- (void)purchaseProUpgrade ;

- (void)completeTransaction:(SKPaymentTransaction *)transaction ;

- (void)purchasedSuccessfulProvideContent ;

- (void)restoreTransaction:(SKPaymentTransaction *)transaction ;

- (void)failedTransaction:(SKPaymentTransaction *)transaction ;


@end

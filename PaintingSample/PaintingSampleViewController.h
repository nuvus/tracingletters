//
//  PaintingSampleViewController.h
//  PaintingSample
//
//  Created by Sean Christmann on 10/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaintView.h"
#import <CeonaBundle/PGView.h>

@interface PaintingSampleViewController : UIViewController<ParentalLockShowDelegate,ParentalLockSuccessDelegate>
{
    UIImageView *alphabetView; 
    NSMutableArray *cgrectInfo;
    int nextButtonTapped ;
    PaintView *paint;
    UIButton *resetButton ;
    
}


-(void)homeButtonPressed;
-(NSMutableDictionary *)getPlistInformation:(NSInteger)level;
@property(nonatomic,retain)PaintView *paint ;
@property(nonatomic,retain) UIButton *resetButton ;
-(void)settingButtonTapped:(id)sender ;
-(void)purchaseButtonPressed;
-(void)disableUI;
-(void)enableUI;
-(void)inAppPurchaseFinished;
@end

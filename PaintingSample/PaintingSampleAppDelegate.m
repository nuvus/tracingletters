//
//  PaintingSampleAppDelegate.m
//  PaintingSample
//
//  Created by Sean Christmann on 10/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PaintingSampleAppDelegate.h"
#import "StartView.h"
#import "SaveData.h"
#import "SmallLettersData.h"

//#import "PaintingSampleViewController.h"

@implementation PaintingSampleAppDelegate

@synthesize window = _window;
@synthesize navigationController = _navigationController;
@synthesize activityIndicator ;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    StartView *viewController = [[StartView alloc] initWithNibName:@"StartView" bundle:nil];
    self.navigationController = [[[UINavigationController alloc]initWithRootViewController:viewController] autorelease];
    self.navigationController.navigationBar.hidden=TRUE;
    self.window.rootViewController = self.navigationController;
    [self.window makeKeyAndVisible];
     if (![[NSUserDefaults standardUserDefaults]boolForKey:@"is_first_time"])
     {
         [SaveData initGameLevel];
         [SmallLettersData initGameLevel];
         [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"is_first_time"];
         [[NSUserDefaults standardUserDefaults]synchronize];

     }
    
    if (![[NSUserDefaults standardUserDefaults]boolForKey:@"LockFromP"])
    {  
        NSLog(@"bool %s", [[NSUserDefaults standardUserDefaults]boolForKey:@"LockFromP"] ? "Yes" : "No"); 
        
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"LockFromP"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        NSLog(@"bool1 %s", [[NSUserDefaults standardUserDefaults]boolForKey:@"LockFromP"] ? "Yes" : "No"); 
        
        
    }
    

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

- (void)dealloc
{
    [_window release];
    //[_viewController release];
    [super dealloc];
}

-(void)showLoadingIndicator{
    if (activityIndicator == nil) {
        self.activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        activityIndicator.center = self.window.center;
        [self.window addSubview:activityIndicator];
        [activityIndicator setHidesWhenStopped:YES];
    }
    [activityIndicator startAnimating];
}

-(void)hideLoadingIndicator{
    [activityIndicator stopAnimating];
}


@end

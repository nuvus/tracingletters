//
//  PaintView.h
//  PaintingSample
//
//  Created by Sean Christmann on 10/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PaintView : UIView 

{
    int arrowCount;
    int numberOfCurve;
    int curveCount;
    int nextButtonTapped ;
    void *cacheBitmap;
    CGContextRef cacheContext;
    float hue;
    int curves;
    int alphabetSequence ;
    
    CGPoint point0;
    CGPoint point1;
    CGPoint point2;
    CGPoint point3;
    
    CGRect drawingRect;
    NSMutableArray *drawingPoints;
    NSDictionary *completeAlphabetData ;
    NSMutableArray *arrowViewReferenceArray;
    NSMutableArray *containsRectArray ;
  
    NSMutableArray *values;
    NSMutableArray *arrowPointsArray;
    UIImageView *imageView;
    UIImageView *alphabetView;
    BOOL enterString;
    
   // UIButton *resetButton ;
    int numberOfRects;
    
    CGSize contextSize;
    int arrowTag;
    int increaseSize;
    int previousIncreaseSize;
    BOOL GreenArrowTouched;
    
}


@property(nonatomic,retain) NSMutableArray *drawingPoints;
@property(nonatomic,retain) NSDictionary *completeAlphabetData;
@property(nonatomic,assign) id delegate ;

- (BOOL) initContext:(CGSize)size;

- (void) drawToCache;
-(void)nextDirection ;
-(NSMutableDictionary *)getPlistInformation:(NSInteger)level ;
-(void)resetButtonPressed ;
-(void)TableViewBackButtonPressed ;
-(void)getAlphabetData;
-(void)createArrowView;
-(void)addAnotherCurve;
-(void)clearColor;
-(void)removeArrowView;
-(void)increaseArrowSize;
@end

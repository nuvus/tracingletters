//
//  AlphabetList.m
//  PaintingSample
//
//  Created by prem on 10/11/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "SmallAlphabetList.h"
#import "CustomView.h"
#import "SmallLettersData.h"
#import "PaintingSampleViewController.h"
#import "PaintView.h"
#import "CeonaIAPManager.h"
#import "EnchantedHelperClass.h"

@implementation SmallAlphabetList

@synthesize AlphabetList ;
@synthesize delegate ;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
   
    alphabetArray = [[NSArray arrayWithObjects:@"a",@"b",@"c",@"d",@"e",@"f",@"g",@"h",@"i",@"j",@"k",@"l",@"m",@"n",@"o",@"p",@"q",@"r",@"s",@"t",@"u",@"v",@"w",@"x",@"y",@"z",
                     nil]retain];
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
   // return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);

}

-(void)dismissView

{
    NSLog(@"DissmissViewEntered");
    NSMutableArray *letterArray=[[SmallLettersData getAlphabetRankValueArray] mutableCopy];
    
    
     if([letterArray count]>0)
     {
          
                  [self.delegate TableViewBackButtonPressed];
           [self.navigationController popViewControllerAnimated:YES];
         
    
  }     
    
    else
    {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:Nil message:@"Select Alphabet" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:Nil];
        [alert show];
        [alert release];
        
    }
}



-(void)clearData

{
    int valueOn;
    if (![[NSUserDefaults standardUserDefaults]boolForKey:@"LockFromP"])
        valueOn=15;
    else
        valueOn=25;  

    if(!clear)
    {     
  
       
        
            for(int i=0;i<=valueOn;i++)
           {
            
            [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:[NSString  stringWithFormat:@"%d %d",i,1]];
            [SmallLettersData deleteArraydata:i];
           }
         clear = TRUE ;
         [self.AlphabetList reloadData];
        
        
    
        
    }
    else
    {
      
     
       
   // NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    //[[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
        
        NSMutableArray *alphabetOnOffArray =[[NSMutableArray alloc]init];
      
         
         for(int i=0;i<=valueOn;i++)
        {
            [alphabetOnOffArray insertObject:[NSNumber numberWithInt:i] atIndex:i]; 
            [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:[NSString  stringWithFormat:@"%d %d",i,1]];
        }
         
        
        [[NSUserDefaults standardUserDefaults]setObject:alphabetOnOffArray forKey:@"game_level1"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        NSMutableArray *levelsArray=[[[NSUserDefaults standardUserDefaults] objectForKey:@"game_level1"]mutableCopy];
        NSLog(@"First SmallletterArrayDescription %@",[levelsArray description]);
        [alphabetOnOffArray release];

        
    clear= FALSE ;  
    [self.AlphabetList reloadData];
    
 
}

}

- (void) switchChanged:(id)sender {
   
    UISwitch* switchControl = sender;
    
    
    if(switchControl.isOn)
    {
        
        [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:[NSString  stringWithFormat:@"%d %d",[sender tag],1]];
        [SmallLettersData setArraydata:[sender tag]];
    }  
    else
    {    
        [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:[NSString stringWithFormat:@"%d %d",[sender tag],1]];
        [SmallLettersData deleteArraydata:[sender tag]];
    
    }
   
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog( @"The switch is %@", switchControl.on ? @"ON" : @"OFF" );
}





# pragma Table Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;

{
    NSLog(@"ALPHABETARRAY Count %d",[alphabetArray count]);
    return  [alphabetArray count]; 
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}// Default is 1 if not implemented


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //static NSString *cellidentifier=@"cell";
     NSString * cellidentifier= [NSString stringWithFormat:@"cellIdentifier %d",[indexPath row]];
    cell=(CustomView *)[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if (cell==Nil) 
    {
        cell=[[[CustomView alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier]autorelease];
    }
    
    
    
    NSString *rowData = [alphabetArray objectAtIndex:indexPath.row]; 
    cell.label.text= rowData ;
 
    [cell.switchView addTarget:self action:@selector(switchChanged:) forControlEvents:
     UIControlEventValueChanged];
    cell.switchView.tag=indexPath.row;
  //  NSLog(@"indexPath.row==> %d",indexPath.row);  
   if ((![[NSUserDefaults standardUserDefaults]boolForKey:@"LockFromP"])&& (indexPath.row)>15)
    {
        cell.switchView.enabled=FALSE ;
    }

      
    if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%d %d",[indexPath row],1]]) 
      
      {
                  
        [cell.switchView setOn:YES];
     
       }
    else
    {  
       
      
        [cell.switchView setOn:NO];
    }
    
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIColor whiteColor]];
    [cell setSelectedBackgroundView:bgColorView];
    [bgColorView release];  
       return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
   if ([EnchantedHelperClass isiPadDevice])
       return  70;
    else
    return 45.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    CGRect headerFrame;
    CGRect buttonFrame;
    CGRect clearButtonFrame;
    int size;
    if ([EnchantedHelperClass isiPadDevice])
    {    
        headerFrame =  CGRectMake(0, 0, 300*2, 70);
        buttonFrame = CGRectMake(10, 14, 50, 45);
        clearButtonFrame =CGRectMake(401*2, 14, 140, 50);
        size =30 ;
    }   
    else
    {    
        headerFrame =  CGRectMake(0, 0, 300, 45); 
        buttonFrame =CGRectMake(7.0, 7.0, 30.0, 30.0); 
        clearButtonFrame =CGRectMake(401.0, 7.0, 70.0, 30.0);
        size=15;
    }

        
    UIView *myView = [[UIView alloc] initWithFrame:headerFrame];
       myView.backgroundColor=[UIColor lightGrayColor];
       
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:[UIImage imageNamed:@"ArrowTurnLeftDown.png"] forState:UIControlStateNormal];
    [button setFrame:buttonFrame];
    [button addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchDown];
    [myView addSubview:button];
    
    
    clearButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    if(!clear) 
    [clearButton setTitle:@"All Off" forState:UIControlStateNormal];
    else
    [clearButton setTitle:@"All On" forState:UIControlStateNormal]; 
    
     [clearButton.titleLabel setFont:[UIFont systemFontOfSize:size]];
    [clearButton setFrame:clearButtonFrame];
    [clearButton addTarget:self action:@selector(clearData) forControlEvents:UIControlEventTouchDown];
    [myView addSubview:clearButton];
        return myView;    
}



@end

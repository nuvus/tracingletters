//
//  EnchantedHelperClass.m
//  Enchanted
//
//  Created by Rahul on 07/05/13.
//
//

#import "EnchantedHelperClass.h"

@implementation EnchantedHelperClass

+(CGFloat)windowHeight{
    static int height = NSNotFound;
    if (height==NSNotFound) {
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        if (screenBounds.size.height == 568) {
            // code for 4-inch screen
            ////NSLog(@"4-inch screen");
            height = 548;
        } else {
            // code for 3.5-inch screen
            ////NSLog(@"3.5-inch screen");
            height = 460;
        }
    }
    return height;
}

+(BOOL)isiPadDevice{
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
}

+(BOOL)isiPhone5Device{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        // code for 4-inch screen
        ////NSLog(@"4-inch screen");
        return YES;
    } else {
        // code for 3.5-inch screen
        ////NSLog(@"3.5-inch screen");
        return NO;
    }
}

+(BOOL)isiPhone4Device{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 480) {
        return YES;
    } else {
        return NO;
    }
}

+(CGFloat)iPh5Factor{
    if ([EnchantedHelperClass isiPhone5Device]) {
        return 88;
    }
    else{
        return 0;
    }
}

+(CGRect)screenSize{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        return CGRectMake(0, 0, 1024, 768);
    }
    else if ([EnchantedHelperClass isiPhone5Device]){
        return CGRectMake(0, 0, 568, 320);
    }
    else{
        return CGRectMake(0, 0, 480, 320);
    }
}

@end

//
//  CustomView.h
//  JsonParsing
//
//  Created by prem on 8/25/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomView : UITableViewCell
{
    
}

@property(nonatomic,retain)UILabel *label;
@property(nonatomic,retain)UISwitch *switchView;

@end

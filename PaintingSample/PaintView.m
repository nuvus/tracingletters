//
//  PaintView.m
//  PaintingSample
//
//  Created by Sean Christmann on 10/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PaintView.h"
#import "EnchantedHelperClass.h"
#import "AlphabetList.h"
#import <QuartzCore/QuartzCore.h>
#import "PaintingSampleViewController.h"
#import "CeonaIAPManager.h"
#import "PaintingSampleAppDelegate.h"

@implementation PaintView

@synthesize drawingPoints;
@synthesize completeAlphabetData;
@synthesize delegate ;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        contextSize=frame.size;
        hue = 0.0;
        [self initContext:frame.size];
        point0 = CGPointMake(0, 0);
		point1 = CGPointMake(50, 100);
		point2 = CGPointMake(100, 50);
		point3 = CGPointMake(150, 100);
        
        
                    
        values = [[NSMutableArray alloc] init];
        arrowPointsArray=[[NSMutableArray alloc]init];
        arrowViewReferenceArray=[[NSMutableArray alloc]init];
        containsRectArray = [[NSMutableArray alloc]init];
        
         CGRect nextButtonFrame ;
         CGRect alphabetFrame  ;
        NSString * alphabetImage ;
        
       // NSDictionary *coordinateDictionary=[self getPlistInformation:0];
       
        NSMutableArray *letterArray ;
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"chooseLetters"]) 
        {
         letterArray=[[[NSUserDefaults standardUserDefaults] objectForKey:@"game_level1"]mutableCopy];   
        }
        else
        {
         
            letterArray=[[[NSUserDefaults standardUserDefaults] objectForKey:@"game_level"]mutableCopy];
        }
        
        NSLog(@"LetterArray Description0 %@",[letterArray description]);
        NSDictionary *coordinateDictionary ;
        
        if([letterArray count]>0)
        {
            int data = [[letterArray objectAtIndex:0]intValue ]; 
            coordinateDictionary=[self getPlistInformation:data];
        }
        
        else
        {
            coordinateDictionary=[self getPlistInformation:0];
        }
        

        
        if ([EnchantedHelperClass isiPadDevice]) 
        
        {    
        
        nextButtonFrame = CGRectMake(135 *2, 375 *2, 40*2, 40*2);
        alphabetFrame = CGRectMake(72, 60, 500, 500);
        NSString * tempName = [coordinateDictionary objectForKey:@"ImageName"];
        alphabetImage = [[tempName stringByDeletingPathExtension] stringByAppendingFormat:@"@2x.%@",[tempName pathExtension]];
         curveCount= [[coordinateDictionary objectForKey:@"NumberOfCurves"]intValue];
            NSLog(@"CurveCount1 %d",curveCount);
    

        }
       
        else
        {    
        nextButtonFrame = CGRectMake(375, 130, 40, 40);
        alphabetFrame = CGRectMake(36, 30 , 250 , 250) ;
        alphabetImage = [coordinateDictionary objectForKey:@"ImageName"];
        curveCount= [[coordinateDictionary objectForKey:@"NumberOfCurves"]intValue];
        NSLog(@"CurveCount1 %d",curveCount);
        
        }  
       
      
    
        

       
        
        alphabetView=[[UIImageView alloc]initWithFrame:alphabetFrame];
        alphabetView.image=[UIImage imageNamed:[coordinateDictionary objectForKey:@"ImageName"]];
        [self addSubview:alphabetView];
        
                   
    }
   
    return self;
}


-(NSMutableDictionary *)getPlistInformation:(NSInteger)level{
    
    NSString *path ;
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"chooseLetters"]) 
    {
        path=[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"PaintingData1"] ofType:@"plist"];
    }
    
    else
    {
        path=[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"PaintingData"] ofType:@"plist"];
    }

    NSArray *levelInfoArray=[[[NSArray alloc] initWithContentsOfFile:path]autorelease];
    NSLog(@"levelInfoArrayDescription %@",levelInfoArray);
    return[levelInfoArray objectAtIndex:level];
    
}

-(void)getAlphabetData

{
  
    
    NSLog(@"completeAlphabetDictionary %@",[completeAlphabetData description]);
    drawingPoints = [[NSMutableArray alloc]initWithArray:[completeAlphabetData objectForKey:@"CgRectInfo"]];
    NSLog(@"drawingXPointsDescription %@ ",[drawingPoints description]);
    
   [self createArrowView];
    
    

    
    
}




-(void)resetButtonPressed
{
    
    NSLog(@"AlphabetSequence %d",alphabetSequence);
    increaseSize=0;
    previousIncreaseSize=0;
    
   

        
    
    NSMutableArray *letterArray ;
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"chooseLetters"])
    {
     
        letterArray=[[[NSUserDefaults standardUserDefaults] objectForKey:@"game_level1"]mutableCopy];   
    }
    else
    {
     
        letterArray=[[[NSUserDefaults standardUserDefaults] objectForKey:@"game_level"]mutableCopy];
    }
        
    NSLog(@"LetterArray Description %@",[letterArray description]);
 
    
    NSDictionary *coordinateDictionary ;
   
   
    [self.delegate resetButton].hidden = TRUE ;
    enterString = FALSE ;
    curves=0;
    NSString * alphabetImage ;
    
        
    if([letterArray count]>0)
    {
         alphabetSequence = alphabetSequence + 1 ;
        int data = [[letterArray objectAtIndex:alphabetSequence]intValue ];
         [self removeArrowView];
         coordinateDictionary = [self getPlistInformation:data];
         self.completeAlphabetData = coordinateDictionary ;
    
    }
    
    else
    {
      /*  nextButtonTapped=nextButtonTapped+1;
        [self removeArrowView];
        coordinateDictionary = [self getPlistInformation:nextButtonTapped];
        self.completeAlphabetData = coordinateDictionary ;*/
 
    }
    
     
    [self getAlphabetData];
    NSLog(@"ImageNameIs==> %@",[coordinateDictionary objectForKey:@"ImageName"]);
    
    if ([EnchantedHelperClass isiPadDevice]) 
        
    {
        
        NSString * tempName = [coordinateDictionary objectForKey:@"ImageName"];
        alphabetImage = [[tempName stringByDeletingPathExtension] stringByAppendingFormat:@"@2x.%@",[tempName pathExtension]];
    }
    
    else
        
    {
        
        alphabetImage = [coordinateDictionary objectForKey:@"ImageName"];  
    }
    
    
    curveCount= [[coordinateDictionary objectForKey:@"NumberOfCurves"]intValue];
    NSLog(@"CurveCount %d",curveCount);
    alphabetView.image = [UIImage imageNamed:alphabetImage];
    self.completeAlphabetData=coordinateDictionary;
    [self clearColor];
    [self getAlphabetData];
 

//}//else end    
    
}

-(void)TableViewBackButtonPressed
{
   
    curves=0;
    alphabetSequence = 0 ;
    nextButtonTapped = 0 ;
    increaseSize=0;
    NSString * alphabetImage ;
    
    NSMutableArray *letterArray ;
   
     if([[NSUserDefaults standardUserDefaults] boolForKey:@"chooseLetters"]) 
     {
        letterArray=[[[NSUserDefaults standardUserDefaults] objectForKey:@"game_level1"]mutableCopy];
     }
    else
    {
       letterArray=[[[NSUserDefaults standardUserDefaults] objectForKey:@"game_level"]mutableCopy]; 
    }
    
    NSLog(@"LetterArray Description:::  %@",[letterArray description]);
    NSDictionary *coordinateDictionary ;
    
    if([letterArray count]>0)
    {
       
        int data = [[letterArray objectAtIndex:0]intValue ];
        [self removeArrowView];
        coordinateDictionary = [self getPlistInformation:data];
        self.completeAlphabetData = coordinateDictionary ;
        
    }
    
    else
    {
       
      /*  [self removeArrowView];
        coordinateDictionary = [self getPlistInformation:nextButtonTapped];
        self.completeAlphabetData = coordinateDictionary ;*/
        
    }
    
       [self getAlphabetData];
    NSLog(@"ImageNameIs==> %@",[coordinateDictionary objectForKey:@"ImageName"]);
    
    if ([EnchantedHelperClass isiPadDevice]) 
        
    {
        
        NSString * tempName = [coordinateDictionary objectForKey:@"ImageName"];
        alphabetImage = [[tempName stringByDeletingPathExtension] stringByAppendingFormat:@"@2x.%@",[tempName pathExtension]];
    }
    
    else
        
    {
        
        alphabetImage = [coordinateDictionary objectForKey:@"ImageName"];  
    }
    
    
    curveCount= [[coordinateDictionary objectForKey:@"NumberOfCurves"]intValue];
    NSLog(@"CurveCount %d",curveCount);
    alphabetView.image = [UIImage imageNamed:alphabetImage];
    self.completeAlphabetData=coordinateDictionary;
    [self clearColor];
    [self getAlphabetData];
    
    
}




-(void)clearColor
{
    CGContextSetRGBFillColor(cacheContext, 1.0, 1.0, 1.0, 1.0);
    CGContextFillRect(cacheContext, (CGRect){CGPointZero, contextSize});
    [self setNeedsDisplay];
    
    
}

- (BOOL) initContext:(CGSize)size {
	
	int bitmapByteCount;
	int	bitmapBytesPerRow;
	
	// Declare the number of bytes per row. Each pixel in the bitmap in this
	// example is represented by 4 bytes; 8 bits each of red, green, blue, and
	// alpha.
	
    bitmapBytesPerRow = (size.width * 4);
	bitmapByteCount = (bitmapBytesPerRow * size.height);
	
	// Allocate memory for image data. This is the destination in memory
	// where any drawing to the bitmap context will be rendered.
	cacheBitmap = malloc( bitmapByteCount );
	if (cacheBitmap == NULL){
		return NO;
	}
	cacheContext = CGBitmapContextCreate (cacheBitmap, size.width, size.height, 8, bitmapBytesPerRow, CGColorSpaceCreateDeviceRGB(), kCGImageAlphaNoneSkipFirst);
    CGContextSetRGBFillColor(cacheContext, 1.0, 1.0, 1.0, 1.0);
    CGContextFillRect(cacheContext, (CGRect){CGPointZero, size});
    
	return YES;
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
   
    UITouch *touch = [touches anyObject];
    CGPoint touchedPoint = [touch locationInView:self];
    NSLog(@"touchedPointX %f touchedPointY %f",touchedPoint.x,touchedPoint.y);
    point0 = CGPointMake(-1, -1);
    point1 = CGPointMake(-1, -1); // previous previous point
    point2 = CGPointMake(-1, -1); // previous touch point
    point3 = [touch locationInView:self]; // current touch point
   
  
    [values replaceObjectAtIndex:0 withObject:@"TRUE"];
           
    NSLog(@"valuesMovedDescription %@",[values description]);
   
    
    
}


- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
 
 UITouch *touch = [touches anyObject];
 CGPoint touchedPoint = [touch locationInView:self];

 point0 = point1;
 point1 = point2;
 point2 = point3;
 point3 = [touch locationInView:self];
 
 NSLog(@"drawingPoints count %d",[drawingPoints count]);
    
    
 
 for (int i=0; i<[drawingPoints count]; i++) 
 {
   
     CGRect touchedRect = [[containsRectArray objectAtIndex:i] CGRectValue];

     if(i>0){
 
     if (CGRectContainsPoint(touchedRect, touchedPoint) && ([[values objectAtIndex:i-1] isEqualToString:[NSString stringWithFormat:@"TRUE"]]))  
    {
 
       [values replaceObjectAtIndex:i withObject:[NSString stringWithFormat:@"TRUE"] ];
        NSLog(@"ValueInArrayDescription %@",[values description]);
        
 
    }
           }
 
 if (CGRectContainsPoint(touchedRect, touchedPoint) && ([[values objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"TRUE"]])) 
  {
 
   
      for (int i =0 ; i<[arrowViewReferenceArray count]-1; i++) 
      {
          UIImageView *greenArrowView=[arrowViewReferenceArray objectAtIndex:i];
                    CGRect touchedRect = greenArrowView.frame;
           NSLog(@"increaseWidth %f increaseHeight %f",greenArrowView.frame.size.width, greenArrowView.frame.size.height);
          
          if (CGRectContainsPoint(touchedRect, touchedPoint)) 
          {
              previousIncreaseSize=increaseSize;
              increaseSize=i+1;
              GreenArrowTouched=TRUE; 
             // NSLog(@"PreviousIncreaseSize %d",previousIncreaseSize); 
             NSLog(@"IncreaseSize %d",increaseSize);
          }
      }     
      
      if (previousIncreaseSize!=increaseSize) 
      {
          
          [self increaseArrowSize];     
      }
      
      [self drawToCache];
       
}
 
}
    
   }

-(void)increaseArrowSize
{
    UIImageView *arrowView=[arrowViewReferenceArray objectAtIndex:increaseSize];
   
      
    arrowView.contentMode=UIViewContentModeCenter;
     
   
    arrowView.frame=CGRectMake(arrowView.frame.origin.x, arrowView.frame.origin.y,25,25); 
    
   [arrowViewReferenceArray replaceObjectAtIndex:increaseSize withObject:arrowView];  
    
        previousIncreaseSize=previousIncreaseSize+1;
}


- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event

{
     for (int i=0; i<[drawingPoints count]; i++) 
     {
        
         
         if ([[values objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"TRUE"]]) 
         {
             
             if(i==[drawingPoints count]-1)
             {
                 [self addAnotherCurve];
                 // NSLog(@"drawingPointsinEnded %@" , [drawingPoints description]);
             }

           }
     }
    
  if (arrowTag != [drawingPoints count])
{
       enterString = TRUE;
        [self nextDirection];    
        [self createArrowView];
        curves=curves + 1;
    NSLog(@"CurvesInEnded %d",curves);
}
  
   if((curves == curveCount - 1)&&[[values lastObject] isEqualToString:[NSString stringWithFormat:@"TRUE"]])
   {   
   
    [self nextDirection];
       NSMutableArray *letterArray ;
       if([[NSUserDefaults standardUserDefaults] boolForKey:@"chooseLetters"]) 
       {
        
           letterArray=[[[NSUserDefaults standardUserDefaults] objectForKey:@"game_level1"]mutableCopy]; 
       }
       else
       {
          letterArray=[[[NSUserDefaults standardUserDefaults] objectForKey:@"game_level"]mutableCopy]; 
       }
     
       if (alphabetSequence<[letterArray count]-1) {
       
       
       
           [self.delegate resetButton].hidden=FALSE;
           
       } 
       else
       {
       
         [self.delegate resetButton].hidden=TRUE;  
      
       }
   }
    
   
  
 }


-(void)addAnotherCurve

{
   
    
    NSMutableArray *nextCurveDataArray = [[NSMutableArray alloc]init];
    if(numberOfCurve == 0)
            
    nextCurveDataArray = [completeAlphabetData objectForKey:@"CgRectInfo1"];
    if (numberOfCurve == 1) {
     
        nextCurveDataArray = [completeAlphabetData objectForKey:@"CgRectInfo2"];
    }
    
    if (numberOfCurve == 2) {
        
        nextCurveDataArray = [completeAlphabetData objectForKey:@"CgRectInfo3"];
    }
   // NSLog(@"nextCurveDataArray %@",[nextCurveDataArray description]);
    numberOfRects=0;
    arrowCount=0;
          
    numberOfCurve = numberOfCurve + 1 ;
    
    [drawingPoints addObjectsFromArray:nextCurveDataArray];
  //  NSLog(@"Updated DrawingPoints Array %@",[drawingPoints description]);
    
    NSLog(@"drawingPointCount %d  valuesCount %d",[drawingPoints count],[values count]);
    
    int extraCounts = [drawingPoints count] - [values count];
    NSLog(@"extracounts %d",extraCounts);    
    for (int i=0; i<extraCounts; i++) 
    
    {
        
        
        [values addObject: [NSString stringWithFormat:@"FALSE"] ];
    
    }
       
  //  NSLog(@"UpdatedValueArray %@ " ,[values description]);
        
      
    
}

- (void) drawToCache {
    
    NSLog(@"IncreaseSize %d",increaseSize);
    if(point1.x > -1){
       // hue += 0.005;
       // if(hue > 1.0) hue = 0.0;
        hue = 0.009;
        UIColor *color = [UIColor colorWithHue:hue saturation:0.7 brightness:1.0 alpha:1.0];
        
        CGContextSetStrokeColorWithColor(cacheContext, [color CGColor]);
        CGContextSetLineCap(cacheContext, kCGLineCapRound);
        CGContextSetLineWidth(cacheContext, 18);
        
        double x0 = (point0.x > -1) ? point0.x : point1.x; //after 4 touches we should have a back anchor point, if not, use the current anchor point
        double y0 = (point0.y > -1) ? point0.y : point1.y; //after 4 touches we should have a back anchor point, if not, use the current anchor point
        double x1 = point1.x;
        double y1 = point1.y;
        double x2 = point2.x;
        double y2 = point2.y;
        double x3 = point3.x;
        double y3 = point3.y;
        // Assume we need to calculate the control
        // points between (x1,y1) and (x2,y2).
        // Then x0,y0 - the previous vertex,
        //      x3,y3 - the next one.
        
        double xc1 = (x0 + x1) / 2.0;
        double yc1 = (y0 + y1) / 2.0;
        double xc2 = (x1 + x2) / 2.0;
        double yc2 = (y1 + y2) / 2.0;
        double xc3 = (x2 + x3) / 2.0;
        double yc3 = (y2 + y3) / 2.0;
        
        double len1 = sqrt((x1-x0) * (x1-x0) + (y1-y0) * (y1-y0));
        double len2 = sqrt((x2-x1) * (x2-x1) + (y2-y1) * (y2-y1));
        double len3 = sqrt((x3-x2) * (x3-x2) + (y3-y2) * (y3-y2));
        
        double k1 = len1 / (len1 + len2);
        double k2 = len2 / (len2 + len3);
        
        double xm1 = xc1 + (xc2 - xc1) * k1;
        double ym1 = yc1 + (yc2 - yc1) * k1;
        
        double xm2 = xc2 + (xc3 - xc2) * k2;
        double ym2 = yc2 + (yc3 - yc2) * k2;
        double smooth_value = 0.8;
        // Resulting control points. Here smooth_value is mentioned
        // above coefficient K whose value should be in range [0...1].
        float ctrl1_x = xm1 + (xc2 - xm1) * smooth_value + x1 - xm1;
        float ctrl1_y = ym1 + (yc2 - ym1) * smooth_value + y1 - ym1;
        
        float ctrl2_x = xm2 + (xc2 - xm2) * smooth_value + x2 - xm2;
        float ctrl2_y = ym2 + (yc2 - ym2) * smooth_value + y2 - ym2;
        
        CGContextMoveToPoint(cacheContext, point1.x, point1.y);
        CGContextAddCurveToPoint(cacheContext, ctrl1_x, ctrl1_y, ctrl2_x, ctrl2_y, point2.x, point2.y);
        CGContextStrokePath(cacheContext);
        
        CGRect dirtyPoint1 = CGRectMake(point1.x-10, point1.y-10, 20, 20);
        CGRect dirtyPoint2 = CGRectMake(point2.x-10, point2.y-10, 20, 20);
        [self setNeedsDisplayInRect:CGRectUnion(dirtyPoint1, dirtyPoint2)];
    }
}

- (void) drawRect:(CGRect)rect 

{
   
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGImageRef cacheImage = CGBitmapContextCreateImage(cacheContext);
    
      CGContextDrawImage(context, self.bounds, cacheImage);
   
     
    
      CGImageRelease(cacheImage);
    
    
}



-(void)createArrowView

{
   
   
        
    NSLog(@"DPC==%d",[drawingPoints count]);
        
    for (int i=arrowTag; i<[drawingPoints count]; i++) 
    
     {
         int X_points;
         int Y_points;
         CGRect arrowFrame ;
         CGRect drawingRects ;
        NSDictionary *coordinate = [drawingPoints objectAtIndex:arrowTag];
        int angle = [[coordinate objectForKey:@"Angle"]intValue];
         if ([EnchantedHelperClass isiPadDevice]){
             
             X_points =  [[coordinate objectForKey:@"x"]intValue]*2 ;  
             Y_points =  [[coordinate objectForKey:@"y"]intValue]*2 ;
             arrowFrame = CGRectMake(X_points, Y_points, 40, 40);
             drawingRects = CGRectMake(X_points, Y_points, 100, 100);
            [containsRectArray addObject:[NSValue valueWithCGRect:drawingRects]];
             

         }
         
         else
         {
        
             X_points =  [[coordinate objectForKey:@"x"]intValue] ;  
            Y_points =  [[coordinate objectForKey:@"y"]intValue] ;
              arrowFrame = CGRectMake(X_points, Y_points, 20 , 20);
            
              drawingRects = CGRectMake(X_points, Y_points, 50, 50);
             [containsRectArray addObject:[NSValue valueWithCGRect:drawingRects]];
            
                         
         }  
         
         //[containsRectArray addObject:[NSValue valueWithCGRect:drawingRects]];
         
         if(!enterString)
         [values insertObject:[NSString stringWithFormat:@"FALSE"] atIndex:arrowTag];
         
         imageView = [[UIImageView alloc] initWithFrame:arrowFrame];
         
        
         
         if ([EnchantedHelperClass isiPadDevice])
         imageView.center= CGPointMake(X_points + (50), Y_points + (50));
         else
         imageView.center= CGPointMake(X_points + (25), Y_points + (25));
         
       
         [imageView setContentMode:UIViewContentModeScaleAspectFit];
    
        
         imageView.image=[UIImage imageNamed:@"GreenArrow.png"];
         imageView.transform = CGAffineTransformMakeRotation(angle * (3.1415/180));
       // NSLog(@"imageViewTagValue %d",arrowCount);
         imageView.tag=arrowTag;
         NSLog(@"iMageViewTag==> %d",imageView.tag); 
         imageView.alpha=0.6;
         [arrowViewReferenceArray addObject:imageView];
         [self addSubview:imageView];
         
         UIImageView *image1 = [[UIImageView alloc]initWithFrame:CGRectMake(X_points , Y_points, 50, 50)];
         [image1 setContentMode:UIViewContentModeScaleAspectFit];
         [image1.layer setBorderColor:[[UIColor blueColor] CGColor]];
         [image1.layer setBorderWidth:3.0];
       //  [self addSubview:image1];
         [image1 release];

        imageView.userInteractionEnabled=FALSE;
        [imageView release];
        arrowCount=arrowCount+1;
        arrowTag=arrowTag+1;
          NSLog(@"ArrowTagValue %d",arrowTag);
          NSLog(@"ArrowCount %d", arrowCount);
    }
   
    NSLog(@"ArrowViewReferenceArrayCount %d",[arrowViewReferenceArray count]);
    

}

-(void)nextDirection
{
      
     NSLog(@"ArrowTagValueD %d",arrowTag);
    NSLog(@"ArrowCountD %d", arrowCount);
    int arrowLeft = arrowTag - arrowCount ;
    if(arrowLeft > 0){
    for (int k = 0 ; k<= [ arrowViewReferenceArray count]-1  ; k ++) 
    {
        UIImageView *arrowView =[arrowViewReferenceArray objectAtIndex:k];
        [arrowView removeFromSuperview]; 
    }
     }
}

-(void)removeArrowView

{
    
   
    NSLog(@"ArrowTagValue %d",arrowTag);
    NSLog(@"numberOfCurv-- %d",numberOfCurve);
    NSLog(@"ArrowViewReferenceArrayCount %d",[arrowViewReferenceArray count]);
  
    for (int i=0; i < arrowTag; i++) 
    {
        
        UIImageView *arrowView =[arrowViewReferenceArray objectAtIndex:i];
        [arrowView removeFromSuperview];
    }
    
    arrowTag = 0 ;
    numberOfCurve = 0;
    arrowCount=0;
    numberOfRects=0;
    enterString = FALSE ;
    [values removeAllObjects];
    [arrowViewReferenceArray removeAllObjects];
    [containsRectArray removeAllObjects];
    [drawingPoints removeAllObjects];

}





@end


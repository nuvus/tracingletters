//
//  SaveData.m
//  PaintingSample
//
//  Created by prem on 10/11/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "SmallLettersData.h"

@implementation SmallLettersData

+(void)initGameLevel{
        
    NSMutableArray *alphabetOnOffArray =[[NSMutableArray alloc]init];
     if (![[NSUserDefaults standardUserDefaults]boolForKey:@"is_first_time"])
     {    
    
         for(int i=0;i<=25;i++)
      {
          if(i<=15){
          [alphabetOnOffArray insertObject:[NSNumber numberWithInt:i] atIndex:i]; 
        [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:[NSString  stringWithFormat:@"%d %d",i,1]];
          }
        else
        {
             [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:[NSString  stringWithFormat:@"%d %d",i,1]];
        }
      }
         
         
    
    }
    
    [[NSUserDefaults standardUserDefaults]setObject:alphabetOnOffArray forKey:@"game_level1"];
       [[NSUserDefaults standardUserDefaults]synchronize];
    NSMutableArray *levelsArray=[[[NSUserDefaults standardUserDefaults] objectForKey:@"game_level1"]mutableCopy];
    NSLog(@"First SmallletterArrayDescription %@",[levelsArray description]);
       [alphabetOnOffArray release];
   
}

+(void)setArraydata:(int)rowNumber 
{
    
    NSMutableArray *letterArray=[[[NSUserDefaults standardUserDefaults] objectForKey:@"game_level1"]mutableCopy];
    [letterArray addObject:[NSNumber numberWithInt:rowNumber]];
          [[NSUserDefaults standardUserDefaults]setObject:letterArray forKey:@"game_level1"];
        [[NSUserDefaults standardUserDefaults]synchronize];
     NSMutableArray *levelsArray=[[[NSUserDefaults standardUserDefaults] objectForKey:@"game_level1"]mutableCopy];
    NSLog(@"letterArrayDescription %@",[levelsArray description]);
    
}
    
+(void)deleteArraydata:(int)rowNumber 
{
    
    NSMutableArray *letterArray=[[[NSUserDefaults standardUserDefaults] objectForKey:@"game_level1"]mutableCopy];
    
    
    
    for(int i=0 ; i<[letterArray count] ; i ++)
    {
        int number = [[letterArray objectAtIndex:i]intValue];
        if ( number == rowNumber) 
        {
           
            [letterArray removeObjectAtIndex:i];
        }
    }
    
    [[NSUserDefaults standardUserDefaults]setObject:letterArray forKey:@"game_level1"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    NSMutableArray *levelsArray=[[[NSUserDefaults standardUserDefaults] objectForKey:@"game_level1"]mutableCopy];
    NSLog(@"letterArrayDescription %@",[levelsArray description]);
    
}

+(NSMutableArray *)getAlphabetRankValueArray
{
    NSMutableArray *letterArray=[[[NSUserDefaults standardUserDefaults] objectForKey:@"game_level1"]mutableCopy];
    return  letterArray ; 

}

@end

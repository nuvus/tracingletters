//
//  PaintingSampleAppDelegate.h
//  PaintingSample
//
//  Created by Sean Christmann on 10/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

//@class StartView;

@interface PaintingSampleAppDelegate : NSObject <UIApplicationDelegate>

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) UINavigationController *navigationController;

@property (retain , nonatomic) UIActivityIndicatorView *activityIndicator;

-(void)showLoadingIndicator ;

-(void)hideLoadingIndicator ;

@end

//
//  PaintingSampleViewController.m
//  PaintingSample
//
//  Created by Sean Christmann on 10/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PaintingSampleViewController.h"
#import "PaintingSampleAppDelegate.h"
#import "PaintView.h"
#import "EnchantedHelperClass.h"
#import "AlphabetList.h"
#import "SmallAlphabetList.h"
#import "CeonaIAPManager.h"
#import <QuartzCore/QuartzCore.h>

@implementation PaintingSampleViewController
@synthesize paint ;
@synthesize resetButton ;

- (void)didReceiveMemoryWarning

{
    // Releases the view if it doesn't have a superview.
    
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.

- (void)viewDidLoad

{
    [super viewDidLoad];
    
    
    NSMutableArray *letterArray ;
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"chooseLetters"]) 
    {    
    letterArray=[[[NSUserDefaults standardUserDefaults] objectForKey:@"game_level1"]mutableCopy];
    NSLog(@"LetterArray Description0 %@",[letterArray description]);
    }
    else
    {
        letterArray=[[[NSUserDefaults standardUserDefaults] objectForKey:@"game_level"]mutableCopy];
        NSLog(@"LetterArray Description0 %@",[letterArray description]);
    }
    NSDictionary *coordinateDictionary ;
    
    if([letterArray count]>0)
    {
        int data = [[letterArray objectAtIndex:0]intValue ]; 
        coordinateDictionary=[self getPlistInformation:data];
    }
    
    else
    {
     coordinateDictionary=[self getPlistInformation:0];
    }
       
    NSLog(@"cgrectInfo %@ cgrectInfoCount %d",[cgrectInfo description],[cgrectInfo count]);
   
     CGRect homeButtonFrame ;
    CGRect settingButtonFrame ;
    CGRect resetButtonFrame ;
    CGRect paintframe ;
    CGRect purchaseButtonFrame ;
    
    if ([EnchantedHelperClass isiPadDevice])
    
    {
        homeButtonFrame = CGRectMake(315 *2 , 55*2, 50*2, 50*2);
        settingButtonFrame = CGRectMake(425 * 2 , 55*2 , 50 *2, 50*2);
        resetButtonFrame = CGRectMake(380*2, 130*2, 40*2, 40*2);
        paintframe = CGRectMake(0, 0, 600, 600);
        purchaseButtonFrame = CGRectMake(homeButtonFrame.origin.x+125, settingButtonFrame.origin.y+305, homeButtonFrame.size.width+10, homeButtonFrame.size.height+10);
        
        
    }   
    
    else
    {   
        homeButtonFrame = CGRectMake(325 , 80, 50, 50);
        settingButtonFrame = CGRectMake(415 , 80 , 50 , 50);
        resetButtonFrame = CGRectMake(383, 165, 45, 45);
        paintframe = CGRectMake(0, 0, 300, 300);
        purchaseButtonFrame = CGRectMake(homeButtonFrame.origin.x+45, settingButtonFrame.origin.y+155, homeButtonFrame.size.width+10, homeButtonFrame.size.height+10);
    }  
    
    
   // paint = [[PaintView alloc] initWithFrame:[[UIScreen mainScreen ] bounds]];
   
    paint = [[PaintView alloc] initWithFrame:paintframe];
    paint.completeAlphabetData = coordinateDictionary ;
    [paint getAlphabetData];
    paint.delegate=self;
    [self.view addSubview:paint];
   
    


    UIButton *homeButton=[UIButton buttonWithType:UIButtonTypeCustom];
    homeButton.frame = homeButtonFrame;
    [homeButton setBackgroundImage:[UIImage imageNamed:@"Home.png"] forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(homeButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:homeButton];
    
       
    UIButton *settingButton=[UIButton buttonWithType:UIButtonTypeCustom];
    settingButton.frame = settingButtonFrame ;
    [settingButton setBackgroundImage:[UIImage imageNamed:@"SettingImage.png"] forState:UIControlStateNormal];
    [settingButton addTarget:self action:@selector(settingButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:settingButton];
    
    resetButton=[UIButton buttonWithType:UIButtonTypeCustom];
    resetButton.frame = resetButtonFrame;
    
    [resetButton setBackgroundImage:[UIImage imageNamed:@"nextArrow@2x.png"] forState:UIControlStateNormal];
    [resetButton addTarget:paint action:@selector(resetButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:resetButton];
    resetButton.hidden=TRUE;
    
    UIButton *purchaseButton=[UIButton buttonWithType:UIButtonTypeCustom];
    purchaseButton.frame = purchaseButtonFrame ;
  
    [purchaseButton setBackgroundImage:[UIImage imageNamed:@"Green-Buy.png"] forState:UIControlStateNormal];
    [purchaseButton addTarget:self action:@selector(purchaseButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:purchaseButton];

}


-(NSMutableDictionary *)getPlistInformation:(NSInteger)level{
        
    NSString *path ;
     if([[NSUserDefaults standardUserDefaults] boolForKey:@"chooseLetters"]) 
     {
       path=[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"PaintingData1"] ofType:@"plist"];
     }
    
     else
     {
       path=[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"PaintingData"] ofType:@"plist"];
     }
        NSArray *levelInfoArray=[[[NSArray alloc] initWithContentsOfFile:path]autorelease];
      NSLog(@"levelInfoArrayDescription %@",levelInfoArray);
        return[levelInfoArray objectAtIndex:level];
  
  }

-(void)homeButtonPressed
{
   // [self dismissModalViewControllerAnimated:YES];
    
    [[UIApplication sharedApplication]setStatusBarHidden:false withAnimation:UIStatusBarAnimationFade];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)settingButtonTapped:(id)sender

{
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"chooseLetters"]) 
    {

   SmallAlphabetList *viewController =[[SmallAlphabetList alloc] initWithNibName:@"SmallAlphabetList" bundle:Nil];
        viewController.delegate = paint;
       // [self presentModalViewController:viewController animated:YES];
          [self.navigationController pushViewController:viewController animated:YES];
          [viewController release];

    }
    
    else
        
    {
      AlphabetList *viewController =[[AlphabetList alloc] initWithNibName:@"AlphabetList" bundle:Nil];  
    
    
    viewController.delegate = paint;
   // [self presentModalViewController:viewController animated:YES];
        [self.navigationController pushViewController:viewController animated:YES];  
    [viewController release];
   
    }
    
}


- (void)viewDidUnload

{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation

{
    // Return YES for supported orientations
    // return (interfaceOrientation == UIInterfaceOrientationPortrait);
     return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);

}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event 
{
    
    UITouch *touch = [touches anyObject];
    CGPoint touchedPoint = [touch locationInView:self.view];
    NSLog(@"touchedPointX %f touchedPointY %f",touchedPoint.x,touchedPoint.y);
    
    
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event 
{
    
    UITouch *touch = [touches anyObject];
    CGPoint touchedPoint = [touch locationInView:self.view];
    NSLog(@"touchedPointX %f touchedPointY %f",touchedPoint.x,touchedPoint.y);
    
}

-(void)purchaseButtonPressed
{
    PGView *pgView = [[PGView alloc]initWithSize:CGSizeMake(260,260) andParentalGate: ParentalGateTypeEnterNumber];
	pgView.delegate = self;
	pgView.showDelegate = self;
	[self.view addSubview:pgView];
	[pgView show];
    [pgView release]; 
    
    
  /*   [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"LockFromP"];
   
    for(int i=16;i<=25;i++)
    {
        [SmallLettersData setArraydata:i]; 
        [SaveData setArraydata:i];
    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:[NSString  stringWithFormat:@"%d",i]];
        [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:[NSString  stringWithFormat:@"%d %d",i,1]];
       
            [[NSUserDefaults standardUserDefaults]synchronize];
    }*/
    

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 11) {
        if (buttonIndex == [alertView cancelButtonIndex]) {
            
        }
        else if (buttonIndex == 1){
            CeonaIAPManager *inAppManager = [[CeonaIAPManager alloc]init];
            [inAppManager initiateInAppPurchaseWithIdentifierSet:[NSSet setWithObject:@"TracingLetters"] withDelegate:self andStartSelector:@selector(disableUI) andFinishSelector:@selector(inAppPurchaseFinished)];
        }
        else if (buttonIndex == 2){
            CeonaIAPManager *inAppManager = [[CeonaIAPManager alloc]init];
            [inAppManager restoreInAppPurchasesDelegate:self andStartSelector:@selector(disableUI) andFinishSelector:@selector(inAppPurchaseFinished)];
        }    }
}

-(void)disableUI
{
	
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
	PaintingSampleAppDelegate * appDel = (PaintingSampleAppDelegate*)[[UIApplication sharedApplication]delegate];
	self.view.userInteractionEnabled= FALSE;
    [appDel showLoadingIndicator];
    
}

-(void)enableUI{
	[[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
	PaintingSampleAppDelegate * appDel = (PaintingSampleAppDelegate*)[[UIApplication sharedApplication]delegate];
	self.view.userInteractionEnabled = TRUE;
    [appDel hideLoadingIndicator];
}

-(void)inAppPurchaseFinished
{
    [self enableUI];
}

#pragma ParentalGate DelegateMethods
- (void) ParentalLockSucceeded: (PGView *)sender
{
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"LockFromP"])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Already Buyied" message:Nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
        [alertView release];
    }
    
    else
        
    {   
        
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Unlock all level" message:Nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Purchase",@"Restore", nil];
        alertView.tag = 11;
        [alertView show];
        [alertView release];
        
        
        
    }
}

- (void) ParentalLockCancelled: (PGView *)sender{
    
}

- (void) ParentalLockWillShow: (PGView *)sender{
    
}

- (void) ParentalLockDidShow: (PGView *)sender{
    
}

- (void) ParentalLockWillHide: (PGView *)sender{
    
}

- (void) ParentalLockDidHide: (PGView *)sender{
    
}


-(void)dealloc

{
    
    cgrectInfo=Nil;
    [cgrectInfo release];
    [super dealloc];
    [paint release];
    
}
@end

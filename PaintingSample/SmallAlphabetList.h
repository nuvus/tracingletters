      //
//  AlphabetList.h
//  PaintingSample
//
//  Created by prem on 10/11/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomView.h"

@interface SmallAlphabetList : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
  //  IBOutlet UITableView *AlphabetList ;
    NSArray *alphabetArray ;
    CustomView *cell ;
    NSMutableArray *switchValueArray ;
    UIButton *clearButton ;
    BOOL clear;
    
}

@property(nonatomic,retain) IBOutlet UITableView *AlphabetList ;
@property(nonatomic , assign) id delegate ;
-(void)dismissView ;
- (void) switchChanged:(id)sender ;
-(void)clearData ;


@end

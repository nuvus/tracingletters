//
//  CeonaIAPManager.m
//  CustomIcons
//
//  Created by Rahul Singh on 26/09/13.
//
//

#import "CeonaIAPManager.h"
#import "SaveData.h"
#import "SmallLettersData.h"

@implementation CeonaIAPManager
@synthesize delegate;
@synthesize validProductsArray;
@synthesize paymentTransactionDetails;
@synthesize productsRequest;


#pragma mark -
#pragma mark SKProductsRequestDelegate methods

+ (BOOL)canMakePurchases{
	return [SKPaymentQueue canMakePayments];
}

- (void)initiateInAppPurchaseWithIdentifierSet:(NSSet *)_productIdsSet withDelegate:(id)_delegate andStartSelector:(SEL)startSelector andFinishSelector:(SEL)finishSelector{
    productsRequest = [[SKProductsRequest alloc]initWithProductIdentifiers:_productIdsSet];
    productsRequest.delegate = self;
    self.delegate = _delegate;
    IAPFinishSelector = finishSelector;
    IAPStartSelector = startSelector;
    [productsRequest start];
    if ([delegate respondsToSelector:IAPStartSelector]) {
        [delegate performSelector:IAPStartSelector];
    };
}

- (void)requestDidFinish:(SKRequest *)request{
    NSLog(@"requestDidFinish");
    if ([validProductsArray count]) {
        [self purchaseProUpgrade];
        if ([delegate respondsToSelector:IAPStartSelector]) {
            [delegate performSelector:IAPStartSelector];
        };
    }
    [productsRequest release];
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error{
    NSLog(@"didFailWithError %@",[error description]);
    if ([delegate respondsToSelector:IAPFinishSelector]) {
        [delegate performSelector:IAPFinishSelector withObject:[NSNumber numberWithBool:NO]];
    };
    if (!failedTransactionPopuped){
        failedTransactionPopuped = true;
        UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:@"Transaction failed" message:[error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertView.tag = 1234321;
        [alertView show];
        [alertView release];
    }
    [productsRequest release];
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
    NSArray *products = response.products;
    self.validProductsArray = [products count] > 0 ? products : nil;
    
    if ([products count]>1) {
        NSLog(@"********* MULTIPLE VALID PRODUCTS HAVE BEEN RECEIVED**UNIMPLEMENTED");
    }
    
    for (SKProduct *proProduct in products) {
        NSLog(@"Product title: %@" , proProduct.localizedTitle);
        NSLog(@"Product description: %@" , proProduct.localizedDescription);
        NSLog(@"Product price: %@" , proProduct.price);
        NSLog(@"Product id: %@" , proProduct.productIdentifier);
    }
    
    for (NSString *invalidProductId in response.invalidProductIdentifiers){
        NSLog(@"Invalid product id: %@" , invalidProductId);
        if ([delegate respondsToSelector:IAPFinishSelector]) {
            [delegate performSelector:IAPFinishSelector withObject:[NSNumber numberWithBool:NO]];
        };
    }
}

- (void)purchaseProUpgrade{
    self.paymentTransactionDetails = [[SKPaymentTransaction alloc] init];
    SKPayment *payment = [SKPayment paymentWithProduct:[validProductsArray objectAtIndex:0]];
    //payment.applicationUsername
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
    if ([delegate respondsToSelector:IAPStartSelector]) {
        [delegate performSelector:IAPStartSelector];
    };
}

#pragma mark -
#pragma mark SKPaymentTransactionObserver methods

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions{
	for (SKPaymentTransaction *transaction in transactions){
        switch (transaction.transactionState){
            case SKPaymentTransactionStatePurchased:
                {
                    NSData* receipt = nil;
                    if ([[NSBundle mainBundle] respondsToSelector:@selector(appStoreReceiptURL)]) {
                        NSURL* receiptURL = [[NSBundle mainBundle] performSelector:@selector (appStoreReceiptURL)];
                        receipt = [NSData dataWithContentsOfURL:receiptURL];
                    }
                    else{
                        receipt = transaction.originalTransaction.transactionReceipt;
                    }
                    
                    [self completeTransaction:transaction];
                }
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
                break;
            default:
                break;
        }
    }
}

#pragma mark -
#pragma mark RestoreMethods

- (void)restoreInAppPurchasesDelegate:(id)_delegate andStartSelector:(SEL)startSelector andFinishSelector:(SEL)finishSelector{
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    self.delegate = delegate;
    IAPFinishSelector = finishSelector;
    IAPStartSelector = startSelector;
    if ([delegate respondsToSelector:IAPStartSelector]) {
        [delegate performSelector:IAPStartSelector];
    };
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue{
    if ([queue.transactions count]) {
        for (SKPaymentTransaction *transaction in queue.transactions){
            [self restoreTransaction:transaction];
        }
    }
    else{
        if ([delegate respondsToSelector:IAPFinishSelector]) {
            [delegate performSelector:IAPFinishSelector withObject:[NSNumber numberWithBool:NO]];
        };
    }
}

-(void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error{
    NSLog(@"Error : %@",error);
    NSLog(@"The transactions : %@",queue.transactions);
    if ([delegate respondsToSelector:IAPFinishSelector]) {
        [delegate performSelector:IAPFinishSelector withObject:[NSNumber numberWithBool:NO]];
    };
}

#pragma mark -
#pragma mark Purchase helpers
- (void)provideContent:(NSString *)productId1{
    [self purchasedSuccessfulProvideContent];
}

//
// removes the transaction from the queue and posts a notification with the transaction result
- (void)finishTransaction:(SKPaymentTransaction *)transaction wasSuccessful:(BOOL)wasSuccessful{
    self.paymentTransactionDetails = transaction;
    // remove the transaction from the payment queue.
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    if ([delegate respondsToSelector:IAPFinishSelector]) {
        [delegate performSelector:IAPFinishSelector  withObject:[NSNumber numberWithBool:wasSuccessful]];
    };
}

//
// called when the transaction was successful
- (void)completeTransaction:(SKPaymentTransaction *)transaction{
    [self finishTransaction:transaction wasSuccessful:YES];
    [self provideContent:transaction.payment.productIdentifier];
}

//
// called when a transaction has been restored and and successfully completed
- (void)restoreTransaction:(SKPaymentTransaction *)transaction{
    [self finishTransaction:transaction wasSuccessful:YES];
    [self provideContent:transaction.originalTransaction.payment.productIdentifier];
}

//
// called when a transaction has failed
- (void)failedTransaction:(SKPaymentTransaction *)transaction{
	NSLog(@"transaction error description %@",[transaction.error localizedDescription]);
    if (transaction.error.code != SKErrorPaymentCancelled){
		if (!failedTransactionPopuped) {
            failedTransactionPopuped = true;
            UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:@"Transaction failed" message:[transaction.error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            alertView.tag = 1234321;
            [alertView show];
            [alertView release];
        }
    }
    [self finishTransaction:transaction wasSuccessful:NO];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 1234321) {
        failedTransactionPopuped = NO;
    }
}

- (void)purchasedSuccessfulProvideContent{
    if (![[NSUserDefaults standardUserDefaults]boolForKey:@"LockFromP"]){
       // [[NSUserDefaults standardUserDefaults]setBool:TRUE forKey:@"LockFromP"];
        //[[NSUserDefaults standardUserDefaults] synchronize];
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"LockFromP"];
        
        for(int i=16;i<=25;i++)
        {
            [SmallLettersData setArraydata:i]; 
            [SaveData setArraydata:i];
            [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:[NSString  stringWithFormat:@"%d",i]];
            [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:[NSString  stringWithFormat:@"%d %d",i,1]];
            
            [[NSUserDefaults standardUserDefaults]synchronize];
        }

        
    }
    
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
    self.validProductsArray = nil;
    self.paymentTransactionDetails = nil;
    self.productsRequest = nil;
    [super dealloc];
}


@end

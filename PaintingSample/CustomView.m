//
//  CustomView.m
//  JsonParsing
//
//  Created by prem on 8/25/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "CustomView.h"
#import "EnchantedHelperClass.h"

@implementation CustomView
@synthesize label;
@synthesize switchView ;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        label=[[UILabel alloc]initWithFrame:CGRectZero];
        label.textColor=[UIColor blackColor];
        label.font =[UIFont fontWithName:@"Noteworthy-Bold" size:30];
        label.lineBreakMode=UILineBreakModeWordWrap;
        label.numberOfLines=2;
        [self.contentView addSubview:label];
        
        
        switchView = [[UISwitch alloc] initWithFrame:CGRectZero];
       [switchView setOn:NO animated:NO];
        [self.contentView addSubview:switchView];
        [switchView release];
        // Initialization code
    }
    return self;
}

-(void)layoutSubviews
{
   
   
    if ([EnchantedHelperClass isiPadDevice])
    {
    switchView.frame=CGRectMake(410*2, 20,50,60);
    label.frame=CGRectMake(20, 0,60, 60);    
    
    }   
    else
    {
      switchView.frame=CGRectMake(370, 20, 20, 20); 
       label.frame=CGRectMake(20, 0,60, 60);  
    }
    
    [super layoutSubviews];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)dealloc
{
    [label release];
    [switchView release];
    [super dealloc];
}
@end

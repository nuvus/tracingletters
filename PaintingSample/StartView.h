//
//  StartView.h
//  PaintingSample
//
//  Created by prem on 10/5/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaintingSampleViewController.h"

@interface StartView : UIViewController
{
    IBOutlet UIButton *startButton;
    IBOutlet UIButton *smallLetterButton ;
    PaintingSampleViewController *viewController;
}
-(IBAction)openGameController:(id)sender ;
-(IBAction)smallLetterPressed:(id)sender ;
-(void)presentPaintingView ;

@end

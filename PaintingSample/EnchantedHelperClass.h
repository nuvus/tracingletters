//
//  EnchantedHelperClass.h
//  Enchanted
//
//  Created by Rahul on 07/05/13.
//
//

#import <Foundation/Foundation.h>


@interface EnchantedHelperClass : NSObject

+(CGFloat)windowHeight;
+(BOOL)isiPhone5Device;
+(CGFloat)iPh5Factor;
+(CGRect)screenSize;
+(BOOL)isiPadDevice;
+(BOOL)isiPhone4Device;

@end

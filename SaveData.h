//
//  SaveData.h
//  PaintingSample
//
//  Created by prem on 10/11/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SaveData : NSObject
{
    
}

+(void)initGameLevel ;
+(void)setArraydata:(int)rowNumber ;
+(void)deleteArraydata:(int)rowNumber ;
+(NSMutableArray *)getAlphabetRankValueArray ;

@end
